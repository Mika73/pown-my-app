<?php

require_once 'DotEnv.php';

(new DotEnv(__DIR__ . '/.env'))->load();

// dev
$database = getenv('DATABASE_DNS');
$databaseUser = getenv('DATABASE_USER');
$databasePassword = getenv('DATABASE_PASSWORD');

try {
  $db = new PDO($database, $databaseUser, $databasePassword);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch (PDOException $e) {
  echo "Connection failed : ". $e->getMessage();
}
