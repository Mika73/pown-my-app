# Pown my app
This application is a simple login program written in PHP, created to study the vulnerabilities of web applications. In particular, XSS, CSRF and SQL Injection.

## How to set up the application
1. Prepare the database.
Execute the following command under the root directory
```
mysql -u username -p < pown.sql
```
2. Start up a web server.
Execute the following command in the root directory
```
php -S localhost:8000
```
3. After launching the server, open your browser and go to the following URL. After sign-up, you can log in.
```
http://localhost:8000/
```

## Description of the git branch
The 'vuln' branch contains a vulnerability. You can play around with it by entering different values in the form.
If you enter the following values in the login screen, you will be able to login illegally.

| User name       | Password     | SQL Query                                                                |
| --------------- | ------------ | ------------------------------------------------------------------------ |
| ' or '1'='1     | ' or '1'='1  | SELECT * FROM users WHERE name='' or '1'='1' and password='' or '1'='1'  |
| ' or ' 1=1      | ' or ' 1=1   | SELECT * FROM users WHERE name='' or ' 1=1' and password='' or ' 1=1'    |
| 1' or 1=1 -- -  | blah　　　　　 | SELECT * FROM users WHERE name='1' or 1=1 -- -' and password='blah'　　  |

The main branch prevents XSS, CSRF, and SQL injection attacks.

### How I dealt with XSS attacks ?
1. Validate the input.
2. Escape the output using htmlspecialchars().

### How I dealt with CSRF attacks ?
1. Use the bin2hex(random_bytes(35)) to generate the one-time token.
2. Embed the token in a hidden field of a form.
3. Check the submitted token with the one stored in the $_SESSION.
### How I dealt with SQL injection attacks ?
1. Validate the input.
2. Use prepared statements and parameterized queries.
